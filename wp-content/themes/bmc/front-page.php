<?php
/**
 * The custom site front page template
 *
 * This is the template that displays the front page.
 * 
 *
 * @link https://developer.wordpress.org/themes/functionality/custom-front-page-templates/
 *
 * @package bmc
 */

get_header();
?>
	<main id="primary" class="site-main">
        <?php if (shortcode_exists( 'smartslider3')) {
            ?>
            <section class="slider-header">
                <?php echo do_shortcode('[smartslider3 slider="3"]');?> 
            </section>
            <?php
        } ?>

        <div class="container-fluid">
            <section class="productos-destacados">
                <h2 class="titulo-seccion">Productos Destacados</h2>
                <div class="swiper-container mySwiper">
                    <div class="swiper-wrapper">
                        <?php get_template_part( 'template-parts/content-destacados'); ?>
                    </div>
                    <div class="slider-navegacion">
                        <div class="swiper-button-prev"></div>
                        <div class="swiper-button-next"></div>
                    </div>
                    <div class="swiper-pagination"></div>
                </div>
            </section>
            <section class="banner-sobre-nosotros">
                <div class="banner-sobre-nosotros__imagen" style="background-image: url('<?php echo esc_url(get_field('imagen_banner_sobre_bmc_inicio')['url']);?>;')">
                </div>
                <div class="banner-sobre-nosotros__texto">
                    <h3><?php the_field('titulo_banner_sobre_bmc_incio'); ?></h3>
                    <p>
                        El deporte del ciclismo está evolucionando a una velocidad 
                        vertiginosa, y nosotros al frente del grupo, empujando el ritmo.
                    </p>
                </div>
            </section>
            <section class="categorias-bicicletas">
                <div class="row">
                    <div class="col-lg-4 mb-4 mb-lg-0">
                        <div class="categoria-bicicleta">
                            <a href="<?php echo get_term_link( 'ruta', 'product_cat' ); ?>">
                                <div class="categoria-bicicleta__texto">
                                    <div class="categoria-bicicleta__titulo">
                                        <p>Ruta</p>
                                        <div class="cruz"></div>
                                    </div>
                                </div>
                                <img class="img-fluid" src=" <?php echo get_template_directory_uri().'/assets/img/ruta-img.webp' ?> " alt="">
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-4 mb-4 mb-lg-0">
                        <div class="categoria-bicicleta">
                            <a href="<?php echo get_term_link( 'montaña', 'product_cat' ); ?>">
                                <div class="categoria-bicicleta__texto">
                                    <div class="categoria-bicicleta__titulo">
                                        <p>Montaña</p>
                                        <div class="cruz"></div>
                                    </div>
                                </div>
                                <img class="img-fluid" src=" <?php echo get_template_directory_uri().'/assets/img/montaña-img.webp' ?> " alt="">
                            </a>
                        </div>

                    </div>
                    <div class="col-lg-4 mb-4 mb-lg-0">
                        <div class="categoria-bicicleta">
                            <a href="<?php echo get_term_link( 'ciudad', 'product_cat' ); ?>">
                                <div class="categoria-bicicleta__texto">
                                    <div class="categoria-bicicleta__titulo">
                                        <p>Ciudad</p>
                                        <div class="cruz"></div>
                                    </div>
                                </div>
                                <img class="img-fluid" src=" <?php echo get_template_directory_uri().'/assets/img/ciudad-img.webp' ?> " alt="">
                            </a>
                        </div>

                    </div>
                </div>
            </section>
            <section class="seccion-novedades">
                <?php get_template_part( 'template-parts/content-novedades');?>
                <p class="text-center mt-1">
                    <a class="btn btn-primary ver-mas-productos" href=" <?php echo get_permalink( wc_get_page_id( 'shop' ));?> ">
                        Ver más productos <i class="fas fa-arrow-right "></i> 
                    </a>
                </p>
            </section>
        </div>

        <script>
            document.addEventListener('DOMContentLoaded', function () {
                var controller = new ScrollMagic.Controller();

                var escena1 = new ScrollMagic.Scene({
                    triggerElement: '.banner-sobre-nosotros',
                    triggerHook: 0.9,    
                })
                .setClassToggle('.banner-sobre-nosotros', 'fade-in-bmc') 
                .addTo(controller);

                var escena2 = new ScrollMagic.Scene({
                    triggerElement: '.categorias-bicicletas',
                    triggerHook: 0.9,    
                })
                .setClassToggle('.categorias-bicicletas', 'fade-in-bmc') 
                .addTo(controller);

                var escena3 = new ScrollMagic.Scene({
                    triggerElement: '.seccion-novedades',
                    triggerHook: 0.9,    
                })
                .setClassToggle('.seccion-novedades', 'fade-in-bmc') 
                .addTo(controller);
            });
        </script>
	</main><!-- #main -->
<?php
get_footer();
