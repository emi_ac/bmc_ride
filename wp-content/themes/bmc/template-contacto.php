<?php
/*
Template Name: Contacto
*/
get_header();
?>	
<div class="pagina-contacto">
	<div class="container-fluid">
		<section class="banner-header">
			<div class="banner-header__bg"></div>
			<div class="banner-header__texto">
				<h1>CONTACTO</h1>
				<p>Estamos aquí para ayudar.</p>
			</div>
			<div class="banner-header__img" style="background-image: url('<?php echo esc_url(get_field('imagen_banner_contacto_')['url']);?>;')"></div>
		</section>
		<?php echo do_shortcode( '[contact-form-7 id="294" title="Formulario de contacto 1"]' ); ?>
	</div>
</div>
<?php
get_footer();
