<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package bmc
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#primary"><?php esc_html_e( 'Skip to content', 'bmc' ); ?></a>

	<header id="masthead" class="site-header sticky-top">
		<nav class="navegacion navbar-expand-xl">
			<div class="container-fluid">
				<div class="navegacion__contenido">
					<div class="navegacion__wrap">
						<button class="navegacion__toggler navbar-toggler d-flex d-xl-none align-items-center" type="button" data-bs-toggle="offcanvas" data-bs-target="#menuOffCanvasMovil" aria-controls="menuOffCanvasMovil">
							<i class="fas fa-bars"></i> 
							<span>Menú</span>
						</button>
						<div class="navegacion__logotipo">
							<?php the_custom_logo(); ?>
						</div>
						<a class="btn-ver-catalogo d-flex d-xl-none btn text-white" href="<?php echo get_permalink( wc_get_page_id( 'shop' ));?>">Catálogo</a>
					</div>
					<div class="collapse navbar-collapse align-items-center" id="navbarSupportedContent">
						<div class="navegacion__menu align-items-center">
							<a class="btn-ver-catalogo btn-primary btn text-white ms-5 me-4 px-3" href="<?php echo get_permalink( wc_get_page_id( 'shop' ));?>">
								Ver Catálogo <img src="<?php echo get_template_directory_uri().'/assets/img/bicicleta.png' ?> ">
							</a>
							<?php
								wp_nav_menu(
									array(
										'theme_location' => 'menu-1',
										'menu_id'        => 'primary-menu',
									)
								);
							?>
						</div>
					</div>		
					<div class="navegacion__buscador">
						<?php aws_get_search_form( true ); ?>
					</div>
				</div>
			</div>
		</nav>

		<div class="navegacion-movil offcanvas offcanvas-start" 
			 tabindex="-1" 
			 id="menuOffCanvasMovil" 
			 aria-labelledby="menuOffCanvasMovil">
			<div class="offcanvas-header justify-content-end py-0">
				<button 
					type="button" 
					class="boton-cerrar btn btn-link p-0" 
					data-bs-dismiss="offcanvas" 
					aria-label="Close">
					<i class="fas fa-times"></i>
				</button>
			</div>
			<div class="offcanvas-body mt-2">
				<?php the_custom_logo(); ?>	
				<a class="btn-ver-catalogo btn btn-primary justify-content-center px-3  mt-5" href="<?php echo get_permalink( wc_get_page_id( 'shop' ));?>">
					Ver Catálogo <img src="<?php echo get_template_directory_uri().'/assets/img/bicicleta.png' ?> ">
				</a>
				<?php
					wp_nav_menu(
						array(
							'theme_location' => 'menu-1',
							'menu_id'        => 'primary-menu',
							'container_class' => 'mt-2',

						)
					);
				?>
			</div>
		</div>
	</header><!-- #masthead -->
