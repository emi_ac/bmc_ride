
//Scroll Barra de navegacion
let navBar = document.querySelector('.navegacion');

document.addEventListener('scroll', () => {
	if (window.scrollY > 10) {
		navBar.classList.add('navegacion_scroll')
	} else {
		navBar.classList.remove('navegacion_scroll')
	}
} )

document.addEventListener('DOMContentLoaded', () => {
	if (window.scrollY > 10) {
		navBar.classList.add('navegacion_scroll')
	} else {
		navBar.classList.remove('navegacion_scroll')
	}
})


// Slider bicicletas destacadas
let categoriasBicicletas = [];
const $sliderProductosDestacados = document.querySelectorAll('.producto-destacado');
const $sliderProductosDestacadosArray = Array.from($sliderProductosDestacados);

$sliderProductosDestacadosArray.forEach((cartel, index) => {
	let titulo = cartel.querySelector('.categoria').textContent;
	categoriasBicicletas.push(titulo);
});
const categoriasLower = categoriasBicicletas.map(categoria => categoria.toLowerCase());

var swiper = new Swiper(".mySwiper", {
	speed: 700,
	navigation: {
		nextEl: '.swiper-button-next',
    	prevEl: '.swiper-button-prev',
	},
	pagination: {
		clickable: true,
		el: ".swiper-pagination",
		renderBullet: function (index, className) {
			return  `<span class="${className}">
						<div class="d-flex flex-column"> 
							<img class="img-fluid" src='${document.URL}/wp-content/themes/bmc/assets/img/${categoriasLower[index]}.png'/>
							<p> ${categoriasLower[index]} </p>
						</div> 
					</span>`;
		}
	},
});


