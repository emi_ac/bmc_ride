<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package bmc
 */

?>

	<footer id="colophon" class="site-footer seccion-footer py-5">
		<div class="container">
			<div class="row align-items-start">
				<div class="col-md-3 d-flex justify-content-center justify-content-lg-start">
					<div class="footer__logo">
						<img src=" <?php echo get_template_directory_uri().'/assets/img/bmc-logo-white.png' ?> " alt="">
					</div>
				</div>
				<div class="col-md-6 d-flex justify-content-center">
					<div>
						<h4>SITIO</h4>
						<?php
							wp_nav_menu(
								array(
									'theme_location' => 'footer_menu',
									'menu_class'     => 'footer-menu',
									'container'      => 'ul',
								)
							);
						?>
					</div>
				</div>
				<div class="col-md-3 d-flex justify-content-center">
					<div>
						<h4>REDES SOCIALES</h4>
						<div class="redes-sociales">
							<a href="https://www.instagram.com/bmc_chile" target="_blank" > <i class="fab fa-instagram me-2" aria-hidden="true"></i> / bmc_chile </a>	
						</div>
					</div>
				</div>
			</div>
		</div>
	</footer>
	<div class="copyright">
	Copyright © 2021 BMC Switzerland AG. Todos los Derechos Reservados | MKR S.A Importador Oficial

	</div>
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
