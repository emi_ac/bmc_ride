<?php
/*
Template Name: Distribuidores
*/

get_header();
?>	
<div class="pagina-distribuidores">
	<div class="container-fluid">
		<section class="banner-header">
			<div class="banner-header__bg"></div>
			<div class="banner-header__texto">
				<h1><?php the_field('titulo_banner'); ?></h1>
				<p>Encuentra los distribuidores oficiales de BMC en Chile</p>
			</div>
			<div class="banner-header__img" style="background-image: url('<?php echo esc_url(get_field('imagen_banner_distribuidores_')['url']);?>;')"></div>
		</section>
		<main>
			<section class="distribuidores-listado">
				<div class="row px-sm-5">
					<div class="col-xxl-3 col-xl-4 col-sm-6">
						<article class="distribuidor">
							<div class="distribuidor__img">
								<img class="distribuidor__img" src=" <?php echo get_template_directory_uri().'/assets/img/logo_extremezone.png'?>" >
							</div>
							<h2 class="distribuidor__nombre">EXTREMEZONE SANTIAGO</h2>
							<p class="distribuidor__direccion">Arauco 130, Valdivia, Chile</p>
							<button class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#modalExtremezoneSantiago">
									Ver más información
							</button>
						</article>
					</div>
					<div class="col-xxl-3 col-xl-4 col-sm-6">
						<article class="distribuidor">
							<div class="distribuidor__img">
								<img class="distribuidor__img" src=" <?php echo get_template_directory_uri().'/assets/img/logo_extremezone.png'?>" >
							</div>
							<h2 class="distribuidor__nombre">EXTREMEZONE LA SERENA</h2>
							<p class="distribuidor__direccion">Huanhuali 991, La Serena.</p>
							<button class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#modalExtremezoneLaSerena">Ver más información</button>
						</article>
					</div>
					<div class="col-xxl-3 col-xl-4 col-sm-6">
						<article class="distribuidor">
							<div class="distribuidor__img">
								<img class="distribuidor__img" src=" <?php echo get_template_directory_uri().'/assets/img/logo_extremezone.png'?>" >
							</div>
							<h2 class="distribuidor__nombre">EXTREMEZONE VALDIVIA</h2>
							<p class="distribuidor__direccion">Arauco 130, Valdivia.</p>
							<button class="btn btn-primary"  data-bs-toggle="modal" data-bs-target="#modalExtremezoneValdivia">Ver más información</button>
						</article>
					</div>
				</div> 
				
			</section>
		</main>
	</div>

	<div class="distribuidor-modal modal fade" id="modalExtremezoneSantiago" tabindex="-1" aria-labelledby="modalExtremezoneSantiago" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered modal-lg">
			<div class="modal-content">
				<div class="modal-header border-0 py-2 pe-2">
					<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
				</div>
				<div class="modal-body px-4 pt-0">
					<div class="row">
						<div class="col-md-6">
							<h3 class="distribuidor-modal__titulo">Extremezone Santiago</h3>
							<ul class="distribuidor-modal__informacion">
								<li> <i class="fas fa-map-marked-alt"></i> Av. Padre Hurtado Norte 1278, Vitacura.</li>
								<li> <i class="fas fa-phone"></i> <a href="tel:+56 2 27063571"> +56 2 27063571</a></li>
								<li> <i class="fas fa-globe"></i> <a href="https://www.extremezone.cl">www.extremezone.cl</a> </li>
								<li> <i class="fas fa-clock"></i> Horario de atención: 10:00 a 19:00 hrs</li>
							</ul>
						</div>
						<div class="col-md-6">
							<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3331.400360158921!2d-70.55222328528733!3d-33.38671770162923!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9662ceb5756ed12f%3A0x3f52aa8891bf0bd6!2sExtremeZone!5e0!3m2!1ses-419!2scl!4v1633430686207!5m2!1ses-419!2scl" 
								width="100%" height="300" style="border:0;" allowfullscreen="" loading="lazy">
							</iframe>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="distribuidor-modal modal fade" id="modalExtremezoneLaSerena" tabindex="-1" aria-labelledby="modalExtremezoneLaSerena" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered modal-lg">
			<div class="modal-content">
				<div class="modal-header border-0 py-2 pe-2">
					<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
				</div>
				<div class="modal-body px-4 pt-0">
					<div class="row">
						<div class="col-md-6">
							<h3 class="distribuidor-modal__titulo">EXTREMEZONE LA SERENA</h3>
							<ul class="distribuidor-modal__informacion">
								<li> <i class="fas fa-map-marked-alt"></i>Huanhuali 991, La Serena</li>
								<li> <i class="fas fa-phone"></i> <a href="tel:+56 2 27063588"> +56 2 27063588</a></li>
								<li> <i class="fas fa-globe"></i> <a href="https://www.extremezone.cl">www.extremezone.cl</a> </li>
								<li> <i class="fas fa-clock"></i> Horario de atención: 10:00 a 19:00 hrs</li>
							</ul>
						</div>
						<div class="col-md-6">
						<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3458.183487908498!2d-71.24705408540612!3d-29.91661933230219!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9691ca5e184d87b1%3A0x24c1c8f956f0e980!2sExtremezone%20Cycles%20La%20Serena!5e0!3m2!1ses-419!2scl!4v1633433481424!5m2!1ses-419!2scl" 
							width="100%" height="300" style="border:0;" allowfullscreen="" loading="lazy">
						</iframe>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="distribuidor-modal modal fade" id="modalExtremezoneValdivia" tabindex="-1" aria-labelledby="modalExtremezoneValdivia" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered modal-lg">
			<div class="modal-content">
				<div class="modal-header border-0 py-2 pe-2">
					<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
				</div>
				<div class="modal-body px-4 pt-0">
					<div class="row">
						<div class="col-md-6">
							<h3 class="distribuidor-modal__titulo">EXTREMEZONE VALDIVIA</h3>
							<ul class="distribuidor-modal__informacion">
								<li> <i class="fas fa-map-marked-alt"></i>Arauco 130, Valdivia</li>
								<li> <i class="fas fa-phone"></i> <a href="tel:+632288288"> 632288288</a></li>
								<li> <i class="fas fa-globe"></i> <a href="https://www.extremezone.cl">www.extremezone.cl</a> </li>
								<li> <i class="fas fa-clock"></i> Horario de atención: 10:00 a 18:30 hrs</li>
							</ul>
						</div>
						<div class="col-md-6">
							<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3064.648982344948!2d-73.24974418462226!3d-39.81486487943958!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9615ef11f0ccf8e1%3A0x100def1ac6a3d87a!2sExtreme%20Zone!5e0!3m2!1ses-419!2scl!4v1633433427254!5m2!1ses-419!2scl" 
								width="100%" height="300" style="border:0;" allowfullscreen="" loading="lazy">
							</iframe>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
get_footer();
