<?php

// Producto destacado ruta 1
$args = array(
    'post_type' => 'product',
    'product_tag' => 'producto-destacado-ruta' ,
    'posts_per_page' => 1,
    'orderby' => 'rand'
);
$loop = new WP_Query( $args );
if ( $loop->have_posts() ) {
    while ( $loop->have_posts() ) : $loop->the_post();
    global $product;
    ?>
        <div class="swiper-slide">
            <article class="producto-destacado">
                <span class="d-none categoria"><?php echo ( get_the_terms( $product->post->ID, 'product_cat' )[0]->name); ?></span>
                <h2 class="producto-destacado__titulo"><?php echo $product->get_name(); ?></h2>
                <a class="producto-destacado__btn btn-primary" href="<?php echo get_permalink( $product->get_id() );?>">Ver producto  <i class="fas fa-arrow-right"></i></a>
                <div class="producto-destacado__imagen">
                    <?php echo wp_get_attachment_image( $product->get_image_id(), array('1100', '620'), "", array( "class" => "img-fluid" ) );?>
                </div>
            </article>
        </div>
        <?php
    endwhile;
} else {

}
wp_reset_postdata(); 


// Producto destacado montaña 1
$args = array(
    'post_type' => 'product',
    'product_tag' => 'producto-destacado-montana' ,
    'posts_per_page' => 1,
    'orderby' => 'rand'
);
$loop = new WP_Query( $args );
if ( $loop->have_posts() ) {
    while ( $loop->have_posts() ) : $loop->the_post();
    global $product;
    ?>
        <div class="swiper-slide">
            <article class="producto-destacado">
                <span class="d-none categoria"><?php echo ( get_the_terms( $product->post->ID, 'product_cat' )[0]->name); ?></span>
                <h2 class="producto-destacado__titulo"><?php echo $product->get_name(); ?></h2>
                <a class="producto-destacado__btn btn-primary" href="<?php echo get_permalink( $product->get_id() );?>">Ver producto  <i class="fas fa-arrow-right"></i></a>
                <div class="producto-destacado__imagen">
                    <?php echo wp_get_attachment_image( $product->get_image_id(), array('1100', '620'), "", array( "class" => "img-fluid" ) );?>
                </div>
            </article>
        </div>
        <?php
    endwhile;
} else {

}
wp_reset_postdata(); 

// Producto destacado ciudad 1
$args = array(
    'post_type' => 'product',
    'product_tag' => 'producto-destacado-ciudad',
    'posts_per_page' => 1,
    'orderby' => 'rand'
);
$loop = new WP_Query( $args );
if ( $loop->have_posts() ) {
    while ( $loop->have_posts() ) : $loop->the_post();
    global $product;
    ?>
        <div class="swiper-slide">
            <article class="producto-destacado">
                <span class="d-none categoria" aria-hidden="true"><?php echo ( get_the_terms( $product->post->ID, 'product_cat' )[0]->name); ?></span>
                <h2 class="producto-destacado__titulo"><?php echo $product->get_name(); ?></h2>
                <a class="producto-destacado__btn btn-primary" href="<?php echo get_permalink( $product->get_id() );?>">Ver producto  <i class="fas fa-arrow-right"></i></a>
                <div class="producto-destacado__imagen">
                    <?php echo wp_get_attachment_image( $product->get_image_id(), array('1100', '620'), "", array( "class" => "img-fluid" ) );?>
                </div>
            </article>
        </div>
        <?php
    endwhile;
} else {

}
wp_reset_postdata(); 

// Producto destacado Ruta 2
$args = array(
    'post_type' => 'product',
    'product_tag' => 'producto-destacado-ruta' ,
    'posts_per_page' => 2,
    'orderby' => 'rand',
    'offset'  => 1,

);
$loop = new WP_Query( $args );
if ( $loop->have_posts() ) {
    while ( $loop->have_posts() ) : $loop->the_post();
    global $product;
    ?>
        <div class="swiper-slide">
            <article class="producto-destacado">
                <span class="d-none categoria"><?php echo ( get_the_terms( $product->post->ID, 'product_cat' )[0]->name); ?></span>
                <h2 class="producto-destacado__titulo"><?php echo $product->get_name(); ?></h2>
                <a class="producto-destacado__btn btn-primary" href="<?php echo get_permalink( $product->get_id() );?>">Ver producto  <i class="fas fa-arrow-right"></i></a>
                <div class="producto-destacado__imagen">
                    <?php echo wp_get_attachment_image( $product->get_image_id(), array('1100', '620'), "", array( "class" => "img-fluid" ) );?>
                </div>
            </article>
        </div>
    <?php
    endwhile;
} else {

}
wp_reset_postdata();                     

// Producto destacado Montaña 2
$args = array(
    'post_type' => 'product',
    'product_tag' => 'producto-destacado-montana' ,
    'posts_per_page' => 2,
    'orderby' => 'rand',
    'offset'  => 1,
);
$loop = new WP_Query( $args );
if ( $loop->have_posts() ) {
    while ( $loop->have_posts() ) : $loop->the_post();
    global $product;
    ?>
        <div class="swiper-slide">
            <article class="producto-destacado">
                <span class="d-none categoria"><?php echo ( get_the_terms( $product->post->ID, 'product_cat' )[0]->name); ?></span>
                <h2 class="producto-destacado__titulo"><?php echo $product->get_name(); ?></h2>
                <a class="producto-destacado__btn btn-primary" href="<?php echo get_permalink( $product->get_id() );?>">Ver producto  <i class="fas fa-arrow-right"></i></a>
                <div class="producto-destacado__imagen">
                    <?php echo wp_get_attachment_image( $product->get_image_id(), array('1100', '620'), "", array( "class" => "img-fluid" ) );?>
                </div>
            </article>
        </div>
    <?php
    endwhile;
} else {

}
wp_reset_postdata();

// Producto destacado ciudad 2
$args = array(
    'post_type' => 'product',
    'product_tag' => 'producto-destacado-ciudad' ,
    'posts_per_page' => 2,
    'orderby' => 'rand',
    'offset'  => 1,
);
$loop = new WP_Query( $args );
if ( $loop->have_posts() ) {
    while ( $loop->have_posts() ) : $loop->the_post();
    global $product;
    ?>
        <div class="swiper-slide">
            <article class="producto-destacado">
                <span class="d-none categoria"><?php echo ( get_the_terms( $product->post->ID, 'product_cat' )[0]->name); ?></span>
                <h2 class="producto-destacado__titulo"><?php echo $product->get_name(); ?></h2>
                <a class="producto-destacado__btn btn-primary" href="<?php echo get_permalink( $product->get_id() );?>">Ver producto  <i class="fas fa-arrow-right"></i></a>
                <div class="producto-destacado__imagen">
                    <?php echo wp_get_attachment_image( $product->get_image_id(), array('1100', '620'), "", array( "class" => "img-fluid" ) );?>
                </div>
            </article>
        </div>
        <?php
    endwhile;
} else {

}
wp_reset_postdata(); 
?>