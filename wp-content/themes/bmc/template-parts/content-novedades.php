<?php
    $args = array(
        'post_type' => 'product',
        'product_tag' => 'productos-inicio' ,
        'posts_per_page' => 8,
        'orderby' => 'rand'
    );
    $productos_inicio = new WP_Query( $args );
    if ( $productos_inicio->have_posts() ) {
        echo '<div class="woocommerce columns-4">';
            echo '<ul class="products columns-4">';
                while ( $productos_inicio->have_posts() ) : $productos_inicio->the_post();
                    wc_get_template_part( 'content', 'product' );
                endwhile;
            echo '</ul>';
        echo '</div>';
    } else {

    }
    wp_reset_postdata();                     
?>