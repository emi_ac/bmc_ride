<?php
/**
 * bmc functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package bmc
 */

if ( ! defined( '_S_VERSION' ) ) {
	// Replace the version number of the theme on each release.
	define( '_S_VERSION', '1.0.0' );
}

if ( ! function_exists( 'bmc_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function bmc_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on bmc, use a find and replace
		 * to change 'bmc' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'bmc', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// Register nav menu
		register_nav_menus(
			array(
				'menu-1' => esc_html__( 'Primary', 'bmc' ),
				'footer_menu' => esc_html__( 'Footer Menu', 'bmc' ),
			)
		);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'style',
				'script',
			)
		);

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support(
			'custom-logo',
			array(
				'height'      => 250,
				'width'       => 250,
				'flex-width'  => true,
				'flex-height' => true,
			)
		);
	}
endif;
add_action( 'after_setup_theme', 'bmc_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function bmc_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'bmc_content_width', 640 );
}
add_action( 'after_setup_theme', 'bmc_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function bmc_widgets_init() {

	register_sidebar(
		array(
			'name'          => esc_html__( 'Sidebar', 'bmc' ),
			'id'            => 'sidebar-1',
			'description'   => esc_html__('Add widgets here.', 'bmc'),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
	register_sidebar(
		array(
			'name'          => esc_html__( 'Sidebar 2', 'bmc' ),
			'id'            => 'sidebar-2',
			'description'   => esc_html__('Add widgets here.', 'bmc'),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
}
add_action( 'widgets_init', 'bmc_widgets_init' );


/**
 * Enqueue scripts and styles.
 */
function bmc_scripts() {
	wp_enqueue_style( 'bmc-bootstrap', get_template_directory_uri() . '/assets/css/bootstrap.min.css', array(), '5.0.2');
	wp_enqueue_style( 'bmc-swiper', get_template_directory_uri() . '/assets/css/swiper-bundle.min.css', array(), '6.7.5');
	wp_enqueue_style( 'bmc-font-awesome', 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css' , array(), '5.15.4');
	wp_enqueue_style( 'fancy-lab-style', get_stylesheet_uri() , array() , _S_VERSION , 'all' );
	wp_style_add_data( 'bmc-style', 'rtl', 'replace' );

	wp_enqueue_script( 'bmc-bootstrap', get_template_directory_uri() . '/assets/js/bootstrap.js', array(), '5.0.2', true);
	wp_enqueue_script( 'bmc-scrollMagic', get_template_directory_uri() . '/assets/js/ScrollMagic.js', array(), '2.0.8', true);
	wp_enqueue_script( 'bmc-swiper', get_template_directory_uri() . '/assets/js/swiper-bundle.min.js', array(), '6.7.5', true);
	wp_enqueue_script( 'bmc-main', get_template_directory_uri() . '/assets/js/main.js', array(), _S_VERSION , true);
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'bmc_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

/**
 * Load WooCommerce compatibility file.
 */
if ( class_exists( 'WooCommerce' ) ) {
	require get_template_directory() . '/inc/woocommerce.php';
}

/**
 * Load WooCommerce Hooks modications file.
 */
if ( class_exists( 'WooCommerce' ) ) {	
	require ( get_template_directory() . '/inc/woocommerce-modifications.php' );
}

/**
 * // Add slug to body class.
 */
function add_slug_body_class( $classes ) {
	global $post;
	if ( isset( $post ) ) {
	$classes[] = 'page'. '-' . $post->post_name;
	}
	return $classes;
}
add_filter( 'body_class', 'add_slug_body_class' );


	
