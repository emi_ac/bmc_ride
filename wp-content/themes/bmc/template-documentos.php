<?php
/*
Template Name: Documentos
*/

get_header();
?>	
<div class="pagina-documentos">
	<div class="container-fluid">
		<section class="banner-header mb-5">
			<div class="banner-header__bg"></div>
			<div class="banner-header__texto">
				<h1><?php the_field('titulo_banner_documentos'); ?></h1>
			</div>
			<div class="banner-header__img" style="background-image: url('<?php echo esc_url(get_field('imagen_banner_documentos')['url']);?>;')"></div>
		</section>
		<div class="row">
			<div class="col-lg-4 mb-5">
				<article class="documento">
					<img class="documento__miniatura img-fluid" src=" <?php echo get_template_directory_uri().'/assets/img/cover-documentos/2018.jpg'?>" alt="Cover catalago 2018">
					<h2 class="documento__titulo text-center">RIDE BMC 2018</h2>
					<div class="documento__archivos">
						<p class="documento__archivo">
							<img class="icono-pdf img-fluid" src="<?php echo get_template_directory_uri().'/assets/img/pdf.svg'?>" alt="">
							<a href="https://bmc.emilianot.design/wp-content/uploads/2021/10/2018-english-catalog.pdf" download >English</a>
						</p>
						<p class="documento__archivo">
							<img class="icono-pdf img-fluid" src="<?php echo get_template_directory_uri().'/assets/img/pdf.svg'?>" alt="">
							<a href="https://bmc.emilianot.design/wp-content/uploads/2021/10/2018-espanol-catalog.pdf" download>Español</a>
						</p>
					</div>
				</article>
			</div>
			<div class="col-lg-4 mb-5">
				<article class="documento">
					<img class="documento__miniatura img-fluid" src=" <?php echo get_template_directory_uri().'/assets/img/cover-documentos/2017.jpg'?>" alt="Cover catalago 2018">
					<h2 class="documento__titulo text-center">RIDE BMC 2017</h2>
					<div class="documento__archivos">
						<p class="documento__archivo">
							<img class="icono-pdf img-fluid" src="<?php echo get_template_directory_uri().'/assets/img/pdf.svg'?>" alt="">
							<a href="https://bmc.emilianot.design/wp-content/uploads/2021/10/2017-english-catalog.pdf" download >English</a>
						</p>
						<p class="documento__archivo">
							<img class="icono-pdf img-fluid" src="<?php echo get_template_directory_uri().'/assets/img/pdf.svg'?>" alt="">
							<a href="https://bmc.emilianot.design/wp-content/uploads/2021/10/2017-espanol-catalog.pdf" download>Español</a>
						</p>
					</div>
				</article>
			</div>
			<div class="col-lg-4 mb-5">
				<article class="documento">
					<img class="documento__miniatura img-fluid" src=" <?php echo get_template_directory_uri().'/assets/img/cover-documentos/2016.jpg'?>" alt="Cover catalago 2018">
					<h2 class="documento__titulo text-center">RIDE BMC 2016</h2>
					<div class="documento__archivos">
						<p class="documento__archivo">
							<img class="icono-pdf img-fluid" src="<?php echo get_template_directory_uri().'/assets/img/pdf.svg'?>" alt="">
							<a href="https://bmc.emilianot.design/wp-content/uploads/2021/10/2016-english-catalog.pdf" download>English</a>
						</p>
						<p class="documento__archivo">
							<img class="icono-pdf img-fluid" src="<?php echo get_template_directory_uri().'/assets/img/pdf.svg'?>" alt="">
							<a href="https://bmc.emilianot.design/wp-content/uploads/2021/10/2016-espanol-catalog.pdf" download>Español</a>
						</p>
					</div>
				</article>
			</div>
			<div class="col-lg-4 mb-5">
				<article class="documento">
					<img class="documento__miniatura img-fluid" src=" <?php echo get_template_directory_uri().'/assets/img/cover-documentos/2015.jpg'?>" alt="Cover catalago 2018">
					<h2 class="documento__titulo text-center">BMC 2015</h2>
					<div class="documento__archivos">
						<p class="documento__archivo">
							<img class="icono-pdf img-fluid" src="<?php echo get_template_directory_uri().'/assets/img/pdf.svg'?>" alt="">
							<a href="https://bmc.emilianot.design/wp-content/uploads/2021/10/2015-english-catalog.pdf" download>English</a>
						</p>
						<p class="documento__archivo">
							<img class="icono-pdf img-fluid" src="<?php echo get_template_directory_uri().'/assets/img/pdf.svg'?>" alt="">
							<a href="https://bmc.emilianot.design/wp-content/uploads/2021/10/2015-espanol-catalog.pdf" download>Español</a>
						</p>
					</div>
				</article>
			</div>
			<div class="col-lg-4 mb-5">
				<article class="documento">
					<img class="documento__miniatura img-fluid" src=" <?php echo get_template_directory_uri().'/assets/img/cover-documentos/2014.jpg'?>" alt="Cover catalago 2018">
					<h2 class="documento__titulo text-center">RBMC 2014</h2>
					<div class="documento__archivos">
						<p class="documento__archivo">
							<img class="icono-pdf img-fluid" src="<?php echo get_template_directory_uri().'/assets/img/pdf.svg'?>" alt="">
							<a href="https://bmc.emilianot.design/wp-content/uploads/2021/10/2014-english-catalog.pdf" download>English</a>
						</p>
						<p class="documento__archivo">
							<img class="icono-pdf img-fluid" src="<?php echo get_template_directory_uri().'/assets/img/pdf.svg'?>" alt="">
							<a href="https://bmc.emilianot.design/wp-content/uploads/2021/10/2014-espanol-catalog.pdf" download>Español</a>
						</p>
					</div>
				</article>
			</div>
			<div class="col-lg-4 mb-5">
				<article class="documento">
					<img class="documento__miniatura img-fluid" src=" <?php echo get_template_directory_uri().'/assets/img/cover-documentos/2013.jpg'?>" alt="Cover catalago 2018">
					<h2 class="documento__titulo text-center">BMC 2013</h2>
					<div class="documento__archivos">
						<p class="documento__archivo">
							<img class="icono-pdf img-fluid" src="<?php echo get_template_directory_uri().'/assets/img/pdf.svg'?>" alt="">
							<a href="https://bmc.emilianot.design/wp-content/uploads/2021/10/2013-english-catalog.pdf" download>English</a>
						</p>
					</div>
				</article>
			</div>
			<div class="col-lg-4 mb-5">
				<article class="documento">
					<img class="documento__miniatura img-fluid" src=" <?php echo get_template_directory_uri().'/assets/img/cover-documentos/2012.jpg'?>" alt="Cover catalago 2018">
					<h2 class="documento__titulo text-center">BMC 2012</h2>
					<div class="documento__archivos">
						<p class="documento__archivo">
							<img class="icono-pdf img-fluid" src="<?php echo get_template_directory_uri().'/assets/img/pdf.svg'?>" alt="">
							<a href="https://bmc.emilianot.design/wp-content/uploads/2021/10/2012-english-catalog.pdf" download>English</a>
						</p>
					</div>
				</article>
			</div>
		</div>
	</div>
</div>
<?php
get_footer();
