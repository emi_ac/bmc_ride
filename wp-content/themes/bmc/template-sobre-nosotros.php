<?php
/*
Template Name: Sobre Nosotros
*/

get_header();
?>	
<div class="pagina-sobre-nosotros">
	<div class="container-fluid">
		<section class="banner-header">
			<div class="banner-header__bg"></div>
			<div class="banner-header__texto">
				<h1>BMC Switzerland</h1>
				<p>Ciclismo suizo, premium y de alto rendimiento</p>
			</div>
			<div class="banner-header__img" style="background-image: url('<?php echo esc_url(get_field('imagen_banner_sobre_nosotros_')['url']);?>;')"></div>
		</section>
		<section class="sobre-nosotros__seccion-1">
			<h2>Ciclismo suizo, premium y de alto rendimiento</h2>
			<p>La intrincada ingeniería suiza es un hecho cuando se habla de BMC Suiza. Este enfoque premium de las bicicletas se remonta a 1994, 
				cuando comenzamos en Grenchen, Suiza, donde todavía estamos alojados hoy. Con solo 120 empleados y tres oficinas globales, puede sorprenderse 
				por el alcance de nuestra innovación. Nuestro laboratorio interno Impec es el corazón de nuestra investigación y desarrollo gracias a algunas 
				de las mentes más brillantes de la industria que nos mantienen a la vanguardia del diseño. Después de un extenso desarrollo, nuestra colección 
				se convierte, naturalmente, en la elección de los mejores atletas del mundo, que continúan ganando carreras como el Tour de Francia, las Pruebas 
				de tiempo por equipos, los Campeonatos mundiales de diversas disciplinas 
				y numerosos Campeonatos Ironman. El deporte del ciclismo está evolucionando a una velocidad vertiginosa, 
				y nosotros al frente del grupo, empujando el ritmo.
			</p>
		</section>
		<section class="sobre-nosotros__seccion-2">
			<div class="row">
				<div class="col-lg-6">
					<div class="sobre-nosotros__seccion-2-texto">
						<h3>BMC es suizo, muy suizo. <br> ¿Pero qué significa eso?</h3>
						<p>Los suizos conocen sus montañas. Este pequeño país del centro de Europa debe mucho a su topografía. Una explicación romántica de su evolución, 
							estas mismas montañas influyeron en el nacimiento de su independencia y dieron forma al estilo de vida de su gente y cultura, creando un paisaje 
							muy diverso pero muy diplomático. Los picos y valles son prácticamente inevitables, pero para un fabricante de bicicletas como BMC proporcionan el 
							lienzo para nuestros sueños. No vemos esas cumbres rocosas como 
							insuperables sino como simples obstáculos, y estamos en una búsqueda perpetua de las mejores formas de superarlas, con orgullo, precisión e 
							ingeniería de calidad.
						</p>
					</div>
				</div>
				<div class="col-lg-6">
					<div class="sobre-nosotros__seccion-2-bg"></div>
				</div>
			</div>
		</section>
		<img class="img-fluid mb-5" src="<?php echo get_template_directory_uri().'/assets/img/sobre-nosotros-3.webp' ?>"  alt=" ">
	</div>
</div>
<?php
get_footer();
