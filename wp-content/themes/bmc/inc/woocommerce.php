<?php
/**
 * WooCommerce Compatibility File
 *
 * @link https://woocommerce.com/
 *
 * @package bmc
 */

/**
 * WooCommerce setup function.
 *
 * @link https://docs.woocommerce.com/document/third-party-custom-theme-compatibility/
 * @link https://github.com/woocommerce/woocommerce/wiki/Enabling-product-gallery-features-(zoom,-swipe,-lightbox)
 * @link https://github.com/woocommerce/woocommerce/wiki/Declaring-WooCommerce-support-in-themes
 *
 * @return void
 */
function bmc_woocommerce_setup() {
	add_theme_support(
		'woocommerce',
		array(
			'thumbnail_image_width' => 300,
			'single_image_width'    => 1050,
			'product_grid'          => array(
				'default_rows'    => 3,
				'min_rows'        => 1,
				'default_columns' => 4,
				'min_columns'     => 1,
				'max_columns'     => 6,
			),
		)
	);
	add_theme_support( 'wc-product-gallery-zoom' );
	add_theme_support( 'wc-product-gallery-lightbox' );
	add_theme_support( 'wc-product-gallery-slider' );
}
add_action( 'after_setup_theme', 'bmc_woocommerce_setup' );


/**
 * Add 'woocommerce-active' class to the body tag.
 *
 * @param  array $classes CSS classes applied to the body tag.
 * @return array $classes modified to include 'woocommerce-active' class.
 */
function bmc_woocommerce_active_body_class( $classes ) {
	$classes[] = 'woocommerce-active';

	return $classes;
}
add_filter( 'body_class', 'bmc_woocommerce_active_body_class' );


/**
 * Related Products Args.
 *
 * @param array $args related products args.
 * @return array $args related products args.
 */
function bmc_woocommerce_related_products_args( $args ) {
	$defaults = array(
		'posts_per_page' => 4,
		'columns'        => 4,
	);

	$args = wp_parse_args( $defaults, $args );

	return $args;
}
add_filter( 'woocommerce_output_related_products_args', 'bmc_woocommerce_related_products_args' );


/**
 * Show cart contents / total Ajax
 */
add_filter( 'woocommerce_add_to_cart_fragments', 'bmc_woocommerce_header_add_to_cart_fragment' );
function bmc_woocommerce_header_add_to_cart_fragment( $fragments ) {
	global $woocommerce;

	ob_start();

	?>
	<a class="navegacion__carrito xoo-wsc-cart-trigger " 
		title="<?php _e('Ver el carrito de compras', 'woothemes'); ?>">
		<i class='bx bxs-cart'></i>
		<span>
			<?php echo $woocommerce->cart->cart_contents_count;?> 
		</span>
	</a>
	<?php
	$fragments['a.navegacion__carrito'] = ob_get_clean();
	return $fragments;
}


/**
 * Display the Woocommerce Discount Percentage on the Sale Badge for variable products and single products
 */
add_filter( 'woocommerce_sale_flash', 'bmc_display_percentage_on_sale_badge', 20, 3 );
function bmc_display_percentage_on_sale_badge( $html, $post, $product ) {

  	if( $product->is_type('variable')){
		$percentages = array();

      	// This will get all the variation prices and loop throughout them
      	$prices = $product->get_variation_prices();

		foreach( $prices['price'] as $key => $price ){
			// Only on sale variations
			if( $prices['regular_price'][$key] !== $price ){
				// Calculate and set in the array the percentage for each variation on sale
				$percentages[] = round( 100 - ( floatval($prices['sale_price'][$key]) / floatval($prices['regular_price'][$key]) * 100 ) );
			}
		}
		// Displays maximum discount value
		$percentage = max($percentages) . '%';

  	} elseif( $product->is_type('grouped') ){
      	$percentages = array();

		// This will get all the variation prices and loop throughout them
		$children_ids = $product->get_children();

		foreach( $children_ids as $child_id ){
			$child_product = wc_get_product($child_id);

			$regular_price = (float) $child_product->get_regular_price();
			$sale_price    = (float) $child_product->get_sale_price();

			if ( $sale_price != 0 || ! empty($sale_price) ) {
				// Calculate and set in the array the percentage for each child on sale
				$percentages[] = round(100 - ($sale_price / $regular_price * 100));
			}
		}
     	// Displays maximum discount value
      	$percentage = max($percentages) . '%';

  	} else {
		$regular_price = (float) $product->get_regular_price();
		$sale_price    = (float) $product->get_sale_price();

	if ( $sale_price != 0 || ! empty($sale_price) ) {
		$percentage = round(100 - ($sale_price / $regular_price * 100)) . '%';
	} else {
		return $html;
	}
  }
  return '<span class="onsale">' . esc_html__( '-', 'woocommerce ' ) . ' '. $percentage . esc_html__( ' DCT', 'woocommerce ' ) . ' '.  '</span>';
}


/**
 * Change add to cart text on single product page
 */
add_filter( 'woocommerce_product_single_add_to_cart_text', 'bmc_add_to_cart_text' ); 
function bmc_add_to_cart_text() {
    echo   'Añadir <i class="bx bxs-cart"></i>';
}

/**
 * Change add to cart text on product archives(Collection) page
 */
add_filter( 'woocommerce_product_add_to_cart_text', 'bmc_product_archives_add_to_cart_text' );  
function bmc_product_archives_add_to_cart_text() {
    return __( 'Añadir', 'woocommerce' );
}