<?php

function bmc_wc_modifications() {

    /**
     *  Generales.
     */
    
        // Cierre y apertura container principal.
        add_action( 'woocommerce_before_main_content', 'bmc_open_main_container', 5);
        function bmc_open_main_container() {
            echo '<div class="container-fluid"> <div class="row">';
        }
        add_action( 'woocommerce_after_main_content', 'bmc_close_main_container', 10);
        function bmc_close_main_container() {
            echo '</div></div>';
        }
        
        // Remover barra lateral de todas las paginas.
        remove_action('woocommerce_sidebar', 'woocommerce_get_sidebar');


    /**
     * Pagina Principal producto.
     */
        if(is_product()) {

            //  Añadir contenedor pagina producto.
            add_action( 'woocommerce_before_main_content', 'bmc_product_page_wrapper_open', 1);
            function bmc_product_page_wrapper_open() {
                echo '<div class="pagina-producto">';
            }
            add_action( 'woocommerce_after_main_content', 'bmc_product_page_wrapper_close', 12);
            function bmc_product_page_wrapper_close() {
                echo '</div>';
            }

            // Añadir boton distribuidores
            add_action( 'woocommerce_single_product_summary', 'bmc_distribuidores', 8 );
            function bmc_distribuidores() {
                ?>
                    <div class="d-none d-lg-block info-distribuidores">
                        <p class="mb-4">* Consulta disponibilidad del producto con tu distribuidor.</p>
                        <button class="btn btn-primary btn-modal-distribuidores" data-bs-toggle="modal" data-bs-target="#modalDistribuidores"> 
                            ¿Deseas el Producto? <i class="fa fa-arrow-right" aria-hidden="true"></i>
                        </button>
                    </div>
                <?php
            };

            // Añadir boton distribuidores movil
            add_action( 'woocommerce_after_single_product_summary', 'bmc_distribuidores_movil', 1 );
            function bmc_distribuidores_movil() {
                ?>
                    <div class="order-3 d-block d-lg-none info-distribuidores mb-5">
                        <p class="mb-4">* Consulta disponibilidad del producto con tu distribuidor.</p>
                        <button class="btn btn-modal-distribuidores" data-bs-toggle="modal" data-bs-target="#modalDistribuidores"> 
                            ¿Deseas el Producto? <i class="fa fa-arrow-right" aria-hidden="true"></i>
                        </button>
                    </div>
                <?php           
            }

            // Añdir modal distribuidores
            add_action( 'wp_footer', 'bmc_distribuidores_modal', 100 );
            function bmc_distribuidores_modal() {
                get_template_part('inc/template-parts/content-modal-distribuidores');
            }

            // Remover SKU Y categorias
            remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );

            // Remover link imagen del producto galeria.
            function bmc_remove_product_image_link( $html, $post_id ) {
                return preg_replace( "!<(a|/a).*?>!", '', $html );
            }
            add_filter( 'woocommerce_single_product_image_thumbnail_html', 'bmc_remove_product_image_link', 10, 2 );

            // Remove title tag from product gallery images.
            add_filter( 'woocommerce_single_product_image_html', 'bmc_remove_product_image_title', 10, 2 );
            function bmc_remove_product_image_title( $html, $post_id ) {
                return preg_replace( '/(title=")(.*?)(")/', '', $html );
            }
    
            // Añadir flechas en la galeria de imagenes del producto.
            add_filter( 'woocommerce_single_product_carousel_options', 'bmc_update_woo_flexslider_options' );
            function bmc_update_woo_flexslider_options( $options ) {
                $options['directionNav'] = true;
                return $options;
            }

            // Desactivar zoom en la galeria del producto.
            add_filter( 'woocommerce_single_product_zoom_options', 'bmc_single_product_zoom_options', 10, 3 );
            function bmc_single_product_zoom_options( $zoom_options ) {
                $zoom_options['magnify'] = 0;
                return $zoom_options;
            }

            // Remover seccion de caracteristicas por defecto de woocommerce
            remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10 );
            remove_action( 'woocommerce_product_additional_information', 'wc_display_product_attributes', 10 );


            // Agregar seccion de caracteristicas producto
            add_action( 'woocommerce_after_single_product_summary', 'bmc_seccion_caracteristicas', 11 );
            function bmc_seccion_caracteristicas() { 
                get_template_part('inc/template-parts/content-caracteristicas-producto');
            }

            // Mostrar productos relacionados solo si no existen ventas cruzadas.
            remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15 );
            remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );
            add_action( 'woocommerce_after_single_product_summary', 'bmc_ventas_cruzadas_productos', 15 );

            function bmc_ventas_cruzadas_productos() {
                global $product;

                if ( isset( $product ) && is_product() ) {
                    $upsells = version_compare( WC_VERSION, '3.0', '<' ) ? $product->get_upsells() : $product->get_upsell_ids();

                    if ( count( $upsells ) > 0 ) {
                        woocommerce_upsell_display();	
                    } else {
                        woocommerce_upsell_display();
                        woocommerce_output_related_products();
                    }
                }
            }
        }

    /**
     * Pagina Tienda y categorias
     */
        if(is_shop() || is_product_category() ) {

            // Añadir contenedor principal
            add_action( 'woocommerce_before_main_content', 'bmc_shop_wrapper_open', 1);
            function bmc_shop_wrapper_open() {
                echo '<div class="pagina-tienda">';
            }
            add_action( 'woocommerce_after_main_content', 'bmc_shop_wrapper_close', 12);
            function bmc_shop_wrapper_close() {
                echo '</div>';
            }

            // Remover breadcrumbs y titulo
            remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20 );
            add_filter( 'woocommerce_show_page_title','__return_false' );
            
            // Añadir banner de tienda, titulo y breadcrumbs
            add_action( 'woocommerce_before_main_content', 'bmc_before_container_content', 3);
            function bmc_before_container_content() {
                $page_id = wc_get_page_id( 'shop' );
                $imagen = get_field( 'imagen_banner_tienda', $page_id );
                ?> 
                    <div class="container-fluid">
                        <section class="banner-header mb-5">
                            <div class="banner-header__bg"></div>
                            <div class="banner-header__texto">
                                <h1><?php the_field('titulo_banner_tienda', $page_id); ?></h1>
                                <p><?php  the_field('bajada_banner_tienda', $page_id); ?></p>
                            </div>
                            <div class="banner-header__img" style="background-image: url('<?php echo esc_url($imagen['url']);?>');"></div>
                        </section>
                        <div class="pagina-tienda__breadcrumb mt-5">
                            <?php woocommerce_breadcrumb(); ?>
                        </div>
                    </div>
                <?php
            }
   
            function is_product_subcategory() {
                $cat = get_query_var( 'product_cat' );
                $category = get_term_by( 'slug', $cat, 'product_cat' );
                return ( $category->parent !== 0 );
            }

            if(!is_product_subcategory() || is_shop()) {

                // cierre y apertura seccion filtros
                add_action( 'woocommerce_before_main_content', 'bmc_open_sidebar_section', 6);
                function bmc_open_sidebar_section() {
                    echo '<div class="col-lg-2 pagina-tienda__sidebar d-none d-lg-block ">';
                }
                add_action( 'woocommerce_before_main_content', 'bmc_close_sidebar_section', 8);
                function bmc_close_sidebar_section() {
                    echo '</div>';
                }
                
                // cierre y apertura seccion productos
                add_action( 'woocommerce_before_main_content', 'bmc_open_products_section', 9);
                function bmc_open_products_section() {
                    echo '<div class="col-lg-10 pagina-tienda__lista-productos">';
                } 
                add_action( 'woocommerce_before_main_content', 'bmc_close_products_section', 11);
                function bmc_close_products_section() {
                    echo '</div>';
                }
    
                // Añadir Boton trigger modal filtros movil
                add_action('woocommerce_after_main_content', 'bmc_btn_filtros_movil');
                function bmc_btn_filtros_movil() {
                    ?>
                        <button type="button" class="btn-filtro-movil d-flex d-lg-none" data-bs-toggle="modal" data-bs-target="#modalFiltrosMovil">
                            <i class='bx bx-filter'></i>
                        </button>
                    <?php
                }
    
                // Añadir filtros escritorio
                add_action( 'woocommerce_before_main_content', 'woocommerce_get_sidebar' ,7);
    
    
                // Añadir modal filtros movil
                add_action( 'wp_footer', 'bmc_modal_filtros', 100 );
                function bmc_modal_filtros() {
                    ?>
                        <div class="modal fade" id="modalFiltrosMovil" tabindex="-1" aria-labelledby="modalFiltrosMovil" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered">
                            <div class="modal-content">
                                <div class="modal-header border-0 pb-0">
                                    <h5 class="modal-title">Filtros</h5>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                </div>
                                <div class="modal-body pt-0">
                                    <div class="pagina-tienda__sidebar-modal">
                                        <?php dynamic_sidebar( 'sidebar-2' ); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </div>
                    <?php
                }
            }

        }


    /**
     * Pagina Subcategorias
     */

        if(is_product_category()) {

            if(is_product_subcategory()) {

                // cierre y apertura seccion productos
                add_action( 'woocommerce_before_main_content', 'bmc_open_subcategories_container', 6);
                function bmc_open_subcategories_container() {
                    echo '<div class="col-lg-12 pagina-tienda__lista-productos">';
                }
                add_action( 'woocommerce_before_main_content', 'bmc_close_subcategories_container', 11);
                function bmc_close_subcategories_container() {
                    echo '</div>';
                }
            }

        }

    /**
     * Redireccionar paginas carrito y mi cuenta a inicio.
    */

        // Redireccionar carrito a inicio
        function bmc_redirect_cart_page() {
            if ( is_page( 'carrito' ) ) {
                wp_safe_redirect( home_url());
                exit;
            }
        }
        add_action( 'template_redirect', 'bmc_redirect_cart_page' );

        // Redireccionar mi cuenta a inicio
        function bmc_redirect_my_account_page() {
            if ( is_page( 'mi-cuenta' ) ) {
                wp_safe_redirect( home_url());
                exit;
            }
        }
        add_action( 'template_redirect', 'bmc_redirect_my_account_page' );
}

add_action( 'wp', 'bmc_wc_modifications' );

