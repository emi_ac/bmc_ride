
    <div class="distribuidor-modal pagina-producto-modal modal fade" id="modalDistribuidores" tabindex="-1" aria-labelledby="modalDistribuidores" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg">
            <div class="modal-content border-0">
                <div class="modal-header border-0 py-2 px-4">
                    <h3>Distribuidores BMC Autorizados</h3>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body px-4 pt-0">

                    <ul class="nav nav-tabs ms-0" id="myTab" role="tablist">
                        <li class="nav-item" role="presentation">
                            <button class="nav-link active" id="home-tab" data-bs-toggle="tab" data-bs-target="#regionMetropolitana" type="button" role="tab" aria-controls="regionMetropolitana" aria-selected="true">Región Metropolitana</button>
                        </li>
                        <li class="nav-item" role="presentation">
                            <button class="nav-link" id="profile-tab" data-bs-toggle="tab" data-bs-target="#regionDeCoquimbo" type="button" role="tab" aria-controls="regionDeCoquimbo" aria-selected="false">Región de Coquimbo</button>
                        </li>
                        <li class="nav-item" role="presentation">
                            <button class="nav-link" id="contact-tab" data-bs-toggle="tab" data-bs-target="#regionDeLosRios" type="button" role="tab" aria-controls="regionDeLosRios" aria-selected="false">Región de los Rios</button>
                        </li>
                    </ul>

                    <div class="tab-content pt-4" id="myTabContent">
                        <div class="tab-pane fade show active" id="regionMetropolitana" role="tabpanel" aria-labelledby="region-metropolitana-tab">
                            <div class="row">
                                <div class="col-lg-6">
                                    <h3 class="distribuidor-modal__titulo">Extremezone Santiago</h3>
                                    <ul class="distribuidor-modal__informacion">
                                        <li> <i class="fas fa-map-marked-alt"></i> Av. Padre Hurtado Norte 1278, Vitacura.</li>
                                        <li> <i class="fas fa-phone"></i> <a href="tel:+56 2 27063571"> +56 2 27063571</a></li>
                                        <li> <i class="fas fa-globe"></i> <a href="https://www.extremezone.cl">www.extremezone.cl</a> </li>
                                        <li> <i class="fas fa-clock"></i> Horario de atención: 10:00 a 19:00 hrs</li>
                                    </ul>
                                </div>
                                <div class="col-lg-6">
                                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3331.400360158921!2d-70.55222328528733!3d-33.38671770162923!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9662ceb5756ed12f%3A0x3f52aa8891bf0bd6!2sExtremeZone!5e0!3m2!1ses-419!2scl!4v1633430686207!5m2!1ses-419!2scl" 
                                        width="100%" height="300" style="border:0;" allowfullscreen="" loading="lazy">
                                    </iframe>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="regionDeCoquimbo" role="tabpanel" aria-labelledby="region-de-coquimbo-tab">
                            <div class="row">
                                <div class="col-lg-6">
                                    <h3 class="distribuidor-modal__titulo">EXTREMEZONE LA SERENA</h3>
                                    <ul class="distribuidor-modal__informacion">
                                        <li> <i class="fas fa-map-marked-alt"></i>Huanhuali 991, La Serena</li>
                                        <li> <i class="fas fa-phone"></i> <a href="tel:+56 2 27063588"> +56 2 27063588</a></li>
                                        <li> <i class="fas fa-globe"></i> <a href="https://www.extremezone.cl">www.extremezone.cl</a> </li>
                                        <li> <i class="fas fa-clock"></i> Horario de atención: 10:00 a 19:00 hrs</li>
                                    </ul>
                                </div>
                                <div class="col-lg-6">
                                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3458.183487908498!2d-71.24705408540612!3d-29.91661933230219!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9691ca5e184d87b1%3A0x24c1c8f956f0e980!2sExtremezone%20Cycles%20La%20Serena!5e0!3m2!1ses-419!2scl!4v1633433481424!5m2!1ses-419!2scl" 
                                        width="100%" height="300" style="border:0;" allowfullscreen="" loading="lazy">
                                    </iframe>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="regionDeLosRios" role="tabpanel" aria-labelledby="regiom-de-los-rios-tab">
                            <div class="row">
                                <div class="col-lg-6">
                                    <h3 class="distribuidor-modal__titulo">EXTREMEZONE VALDIVIA</h3>
                                    <ul class="distribuidor-modal__informacion">
                                        <li> <i class="fas fa-map-marked-alt"></i>Arauco 130, Valdivia</li>
                                        <li> <i class="fas fa-phone"></i> <a href="tel:+632288288"> 632288288</a></li>
                                        <li> <i class="fas fa-globe"></i> <a href="https://www.extremezone.cl">www.extremezone.cl</a> </li>
                                        <li> <i class="fas fa-clock"></i> Horario de atención: 10:00 a 18:30 hrs</li>
                                    </ul>
                                </div>
                                <div class="col-lg-6">
                                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3064.648982344948!2d-73.24974418462226!3d-39.81486487943958!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9615ef11f0ccf8e1%3A0x100def1ac6a3d87a!2sExtreme%20Zone!5e0!3m2!1ses-419!2scl!4v1633433427254!5m2!1ses-419!2scl" 
                                        width="100%" height="300" style="border:0;" allowfullscreen="" loading="lazy">
                                    </iframe>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>