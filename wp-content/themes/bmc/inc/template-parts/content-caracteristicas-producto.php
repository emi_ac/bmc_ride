<div class=" detalles-tecnicos d-inline-block w-100">
    <div class="row">
        <div class="col-lg-3">
            <div class="detalles-tecnicos__sidebar sticky-top">
                <img class="img-fluid w-100" src="<?php echo get_template_directory_uri();?>/assets/img/technical-details-bg.webp" alt=" ">
                <h2>Detalles Técnicos</h2>
            </div>
        </div>
        <div class="col-lg-9">
            <div class="detalles-tecnicos__contenido">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item" role="presentation">
                        <button class="nav-link active" id="comentarios-generales-tab" data-bs-toggle="tab" data-bs-target="#comentariosGenerales" type="button" role="tab" aria-controls="comentariosGenerales" aria-selected="true">
                            Comentarios generales
                        </button>
                    </li>
                    <li class="nav-item" role="presentation">
                        <button class="nav-link" id="geomometria-tab" data-bs-toggle="tab" data-bs-target="#geometria" type="button" role="tab" aria-controls="geometria" aria-selected="false">
                            Geometría
                        </button>
                    </li>
                </ul>
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="comentariosGenerales" role="tabpanel" aria-labelledby="comentariosGenerales-tab">
                        <?php
                            $tabla_especificaciones_tecnicas = get_field( 'especificaciones_tecnicas' );

                            if ( ! empty ( $tabla_especificaciones_tecnicas ) ) {

                                echo '<table class="especificaciones-bicicleta__tabla" border="0">';
                                    if ( ! empty( $tabla_especificaciones_tecnicas['caption'] ) ) {
                                        echo '<caption>' . $tabla_especificaciones_tecnicas['caption'] . '</caption>';
                                    }

                                    if ( ! empty( $tabla_especificaciones_tecnicas['header'] ) ) {
                                        echo '<thead>';

                                            echo '<tr>';
                                                foreach ( $tabla_especificaciones_tecnicas['header'] as $th ) {
                                                    echo '<th>';
                                                        echo $th['c'];
                                                    echo '</th>';
                                                }
                                            echo '</tr>';

                                        echo '</thead>';
                                    }

                                    echo '<tbody>';
                                        foreach ( $tabla_especificaciones_tecnicas['body'] as $tr ) {

                                            echo '<tr>';
                                                foreach ( $tr as $td ) {

                                                    $result = array_map('trim',  explode('•', $td['c']));
                                                    

                                                    echo '<td> <ul>';
                                                        
                                                        for ($i=0; $i < count($result); $i++) {
                                                            echo '<li>'.$result[$i].'</li>';
                                                        }
                                                        
                                                    echo ' </ul></td>';
                                                }
                                            echo '</tr>';
                                        }
                                    echo '</tbody>';
                                echo '</table>';
                            }
                        ?>
                    </div>
                    <div class="tab-pane fade" id="geometria" role="tabpanel" aria-labelledby="geometria-tab">
                        <div class="detalles-tecnicos__geometria">
                            <?php
                                $imagen_geometria_bicicleta = get_field( 'imagen_geometria_bicicleta_');
                                $image = get_field('imagen_tabla_geometria_');                     
                            ?>
                            <img class="geometria-bicicleta__img img-fluid my-4" src="<?php echo esc_url($imagen_geometria_bicicleta['url']);?>" alt=" imagen geometria bicicleta">
                            <img class="img-fluid d-block mx-auto" src="<?php echo esc_url($image['url']);?>" alt=" tabla datos geometria bicicleta">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

